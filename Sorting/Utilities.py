global counter
counter = 0


def swap(input_list, j, i):
    global counter
    temp = input_list[j]
    input_list[j] = input_list[i]
    input_list[i] = temp
    counter = counter + 1


def get_global_counter():
    return counter


def get_input_string():
    array_unsorted = [2, 21, 4, 200, 31, 9, 100, 22, 3, 14, 1, 29, 45, 33, 12]
    return array_unsorted


''' Split the Array into 2 parts with target index as center
    And then stitches back [part 1] + key + [part2]'''


def insert_key(input_list, key, target_index, current_index):
    list1 = input_list[:target_index]
    list2 = input_list[target_index:current_index]
    list3 = input_list[current_index + 1:]
    list1.append(key)
    for i in list2:
        list1.append(i)
    # list1.append(list2)
    for j in list3:
        list1.append(j)
    # list1.append(list3)
    for k in range(0, len(list1)):
        input_list[k] = list1[k]
    return input_list


''' Copies last element of the array to last+1 position
    and proceeds to do the same for all positions from last till target index position
    Finally inserts the key to target position'''


def insert_key_regular(input_list, key, target_index, current_index):
    j = current_index
    while j > target_index:
        input_list[j] = input_list[j - 1]
        j = j - 1
    input_list[target_index] = key
    return input_list
