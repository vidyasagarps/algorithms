from Utilities import get_input_string


def merge_sort(input_list):
    if len(input_list) < 2:
        return input_list[0:1]
    list1 = merge_sort(input_list[:len(input_list) / 2])
    list2 = merge_sort(input_list[len(input_list) / 2:])
    return merge(list1, list2)


def merge(list1, list2):
    i = 0
    j = 0
    list3 = []
    while i < len(list1) and j < len(list2):
        if list1[i] > list2[j]:
            list3.append(list2[j])
            j = j + 1
        else:
            list3.append(list1[i])
            i = i + 1

    if i < len(list1):
        list3 = list3 + list1[i:]
    else:
        list3 = list3 + list2[j:]
    return list3


list_input = get_input_string()
print(merge_sort(list_input))
