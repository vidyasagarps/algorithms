from Sorting.Utilities import swap, get_input_string

def insertion_sort(input_array):
    list_len = len(input_array)

    for i in range(1, list_len):

        j = i
        while input_array[j] < input_array[j - 1] and j > 0:
            swap(input_array, j, j - 1)
            j = j - 1

    return input_array


unsorted_chars = get_input_string()
print(insertion_sort(unsorted_chars))



