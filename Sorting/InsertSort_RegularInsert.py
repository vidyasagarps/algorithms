from Sorting.Utilities import get_input_string, insert_key_regular


def insertion_sort(input_list):
    list_len = len(input_list)

    for i in range(1, list_len):
        j = i
        min = j
        while j > 0:
            if input_list[i] < input_list[j - 1]:
                min = j - 1
                j = j - 1
                continue
            j = j - 1
        if min != i:
            insert_key_regular(input_list, input_list[i], min, i)
    return input_list


unsorted_list = get_input_string()
print(insertion_sort(unsorted_list))
