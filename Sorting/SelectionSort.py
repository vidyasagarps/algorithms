from Sorting.Utilities import swap, get_input_string


def selection_sort(input_array):
    list_len = len(input_array)

    for i in range(0, list_len - 1):
        mini = i
        for j in range(i + 1, list_len):
            if input_array[j] < input_array[mini]:
                mini = j

        if mini != i:
            swap(input_array, i, mini)

    return input_array


unsorted_chars = get_input_string()
print(selection_sort(unsorted_chars))
