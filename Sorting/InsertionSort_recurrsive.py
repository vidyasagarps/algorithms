from Sorting.Utilities import get_input_string, insert_key


class InsertionSortRecursive:

    def __init__(self, input_array):
        self.input_array = input_array

    def sort(self):
        self.insertion_sort(0)
        return self.input_array

    def insertion_sort(self, current_index):
        key = self.input_array[current_index]
        min_index = current_index
        j = current_index
        while j > 0:
            if self.input_array[current_index] < self.input_array[j - 1]:
                min_index = j - 1
                j = j - 1
                continue
            j = j - 1
        if min_index <> current_index:
            insert_key(self.input_array, key, min_index, current_index)
        while current_index < len(self.input_array) -1:
            self.insertion_sort(current_index + 1)
            current_index = current_index + 1
        return self.input_array


unsorted_string = get_input_string()
recursive_sorter = InsertionSortRecursive(unsorted_string)
print(recursive_sorter.sort())
