from Sorting.Utilities import get_input_string, swap


class QuickSort:

    def __init__(self, input_list):
        self.input_list = input_list

    def sort(self):
        low = 0
        high = len(self.input_list) - 1
        self.partition(low, high)
        return self.input_list

    def partition(self, low, high):
        if low >= high:
            return self.input_list
        pivot = self.input_list[high]  # set high as pivot's position
        wall = low
        i = low
        while i <= high - 1:
            if self.input_list[i] < pivot:
                swap(self.input_list, i, wall)
                wall = wall + 1
                i = i + 1
                continue
            i = i + 1
        swap(self.input_list, high, wall)  # finally wall is going to the pivot's position
        self.partition(low, wall - 1)
        self.partition(wall + 1, high)
        return self.input_list


unsorted_string = get_input_string()
sorter = QuickSort(unsorted_string)
print(sorter.sort())
