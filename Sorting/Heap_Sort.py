from Sorting.Utilities import get_input_string, swap, get_global_counter

'''
A heap is a binary tree structure where a each parent can have at-most 2 children nodes
In case of a max heap structure, value of a parent node is always greater than its children nodes' values
Heap though is laid out as an array/list in which case, a parent for any node i can be looked-up by (i-1)/2
First and Second child of any parent node i can be looked up as (i*2)+1 and (i*2)+2 
'''


class HeapSort:

    def __init__(self, input_list):
        self.input_list = input_list
        self.heap_list = []

    def construct_max_heap(self):
        for i in range(0, len(self.input_list)):
            self.push(i)
        self.evaluate()
        return self.heap_list

    def push(self, index):
        self.heap_list.append(self.input_list[index])
        # self.evaluate()

    '''
    This enforces heap structure
    '''

    def evaluate(self):
        heap_len = len(self.heap_list)
        if heap_len > 1:
            i = heap_len - 1
            while i > 0:
                par = (i - 1) / 2
                if self.heap_list[par] < self.heap_list[i]:
                    # self.heap_list[par], self.heap_list[i] = self.heap_list[i], self.heap_list[par]
                    swap(self.heap_list, par, i)
                i = i - 1

    '''
    Root item is removed, returned and is replaced by the last item in heap
    '''

    def pop(self):
        key = self.heap_list[0]
        self.heap_list[0], self.heap_list[len(self.heap_list) - 1] \
            = self.heap_list[len(self.heap_list) - 1], self.heap_list[0]
        self.heap_list = self.heap_list[:-1]
        self.evaluate()
        return key

    def sort(self):
        sorted_desc_list = []
        sorted_list = []
        heap_len = len(self.heap_list)
        for i in range(0, heap_len):
            key = self.pop()
            sorted_desc_list.append(key)

        j = len(sorted_desc_list) - 1
        while j >= 0:
            sorted_list.append(sorted_desc_list[j])
            j = j - 1
        return sorted_list


unsorted_string = get_input_string()
sorter = HeapSort(unsorted_string)
print(unsorted_string)
print(sorter.construct_max_heap())
print(sorter.sort())
print(get_global_counter())
