from Sorting.Utilities import get_input_string, insert_key


def insertion_sort(input_list):
    list_len = len(input_list)

    for i in range(1, list_len):
        j = i
        min_index = j
        while j > 0:
            if input_list[i] < input_list[j - 1]:
                min_index = j - 1
                j = j - 1
                continue
            j = j - 1
        if min_index != i:
            insert_key(input_list, input_list[i], min_index, i)
    return input_list


unsorted_list = get_input_string()
print(insertion_sort(unsorted_list))
